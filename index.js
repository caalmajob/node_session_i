const express = require('express');

const PORT = 3000;

const server = express();


// server.use('/', (request, response) => {
//     response.send( 'Hello Upgraders');
// })


// server.use('/movies', (req, res) => {
//     const movies = ('Harry Potter', 'Titanic', 'Tenet');

//     res.send(movies);
// });

// server.use('/', (req, res) => {
//     res.send('Home Page');
// });

const router = express.Router();


router.get('/movies', (req, res) => {
    const movies = ['Harry Potter', 'Titanic', 'Tenet'];

    const { name } = req.query;


    if(!name){
        return res.send(movies);
    }
const hasMovie = movies.some((movie) => movie.toLowerCase() === name.toLowerCase());

    if(hasMovie) {
        return res.send(`Yes, we have a movie with the name: ${name}`);
    } else {
        return res.send(`No, we don't have a movie called ${name}`);
    }
});


router.get('/', (req, res) => {
    res.send('Home Page');
});

server.use('/', router);

server.listen(PORT, () => {
    console.log(`server runnig in http://localhost:${PORT}`);
});